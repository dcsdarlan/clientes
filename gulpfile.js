var gulp = require('gulp');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var cleancss = require('gulp-clean-css');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var php = require('gulp-connect-php');


var paths = {
    scripts: {
        src: 'resources/assets/js/*.js',
        dest: 'public/js'
    },
    styles: {
        src: 'resources/assets/scss/*.scss',
        dest: 'public/css'
    },
    images: {
        src: 'resources/assets/img/*',
        dest: 'public/img'
    }
};

gulp.task('scripts', function() {
    // scripts
    return gulp
        .src([paths.scripts.src])
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(uglify({
            mangle: false
        }))
        .pipe(gulp.dest(paths.scripts.dest));
});

gulp.task('styles', function() {
    // css
    return gulp
        .src([paths.styles.src])
        .pipe(sass({
            style: 'compressed'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(cleancss())
        .pipe(gulp.dest(paths.styles.dest));
});

gulp.task('images', function() {
    // img
    return gulp
        .src([paths.images.src])
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [
                {removeViewBox: false},
                {cleanupIDs: false}
            ],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(paths.images.dest));
});

gulp.task('watch', function() {
    // Watch .js files
    gulp.watch(paths.scripts.src, ['scripts']);
    // Watch .scss files
    gulp.watch(paths.styles.src, ['styles']);
    // Watch image files
    gulp.watch(paths.images.src, ['images']);

    php.server({
        base: './public'
    });
    return;
});
