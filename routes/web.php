<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/clientes', function () {
    return view('welcome');
});
Route::group(['prefix' => 'client'], function() {
    Route::get('/list', [
        'as' => 'client.list',
        'uses' => 'ClientController@listClient'
    ]);
    Route::get('/add', [
        'as' => 'client.add',
        'uses' => 'ClientController@addClient'
    ]);
});