<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {
  public $timestamps = false;

  public function fisical_person(){
    return $this->hasOne("App\FisicalPerson",'id','id_client');
  }
  public function legal_person(){
    return $this->hasOne("App\LegalPerson",'id','id_client');
  }
  public function email(){
    return $this->hasMany("App\Email",'id_client','id');
  }
  public function address(){
    return $this->hasMany("App\Address",'id_client','id');
  }
}
