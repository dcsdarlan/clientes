<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class ClientController extends Controller {

  use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

  public function listClient(Request $request) {
    $dados = array();
    return view('client.list', $dados);
  }
  public function addClient(Request $request) {
    $dados = array();
    return view('client.form', $dados);
  }
  public function changeClient(Request $request) {
    $dados = array();
    return view('client.list', $dados);
  }
  public function saveClient(Request $request) {
    $dados = array();
    return view('client.list', $dados);
  }
  public function deleteClient(Request $request) {
    $dados = array();
    return view('client.list', $dados);
  }
  public function excludeClient(Request $request) {
    $dados = array();
    return view('client.list', $dados);
  }
}
