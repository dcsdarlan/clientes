<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FisicalPerson extends Model {
  public $timestamps = false;    
}
