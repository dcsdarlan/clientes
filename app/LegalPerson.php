<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LegalPerson extends Model {
    public $timestamps = false;
}
