$(document).ready(function() {
    $('#type').change(function() {
        if($(this).val() == "fisical") {
            $('#span-document').text("CPF");
            $('#document').mask('000.000.000-00', {reverse: true});
        } else {
            $('#span-document').text("CNPJ");
            $('#document').mask('00.000.000/0000-00', {reverse: true});
        }
        $('#group-document').fadeIn();
    });
});
