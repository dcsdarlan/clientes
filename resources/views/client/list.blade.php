@extends('template')
@section('content')
<div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
        <div class="x_title">
            <h2>Clientes <small>Lista</small></h2>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li><a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
            <a href="{{route('client.add')}}" class="btn btn-primary navbar-right"><i class="fa fa-plus"></i>  Adicionar</a>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td scope="row">1</td>
                    <td>Mark</td>
                    <td>
                      <button type="button" class="btn btn-danger pull-right"><i class="fa fa-trash"></i>  Apagar</button>
                      <button type="button" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i>  Editar</button>
                    </td>

                </tr>
                <tr>
                    <td scope="row">2</td>
                    <td>Jacob</td>
                    <td>
                      <button type="button" class="btn btn-danger pull-right"><i class="fa fa-trash"></i>  Apagar</button>
                      <button type="button" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i>  Editar</button>
                    </td>

                </tr>
                <tr>
                    <td scope="row">3</td>
                    <td>Larry</td>
                    <td>
                      <button type="button" class="btn btn-danger pull-right"><i class="fa fa-trash"></i>  Apagar</button>
                      <button type="button" class="btn btn-warning pull-right"><i class="fa fa-pencil"></i>  Editar</button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@stop
