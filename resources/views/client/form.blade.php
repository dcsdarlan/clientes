@extends('template')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cliente</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                        <li><a class="close-link"><i class="fa fa-close"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Nome <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Tipo <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="type" required="required" class="form-control col-md-7 col-xs-12">
                                    <option>-- Selecione --</option>
                                    <option value="fisical">Pessoa Física</option>
                                    <option value="legal">Pessoa Jurídica</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" id="group-document">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" id="label-document" for="document"><span id="span-document">Documento</span><span class="required"> *</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="document" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div>
                          <div class="x_content">
                              <table class="table table-striped">
                                  <thead>
                                  <tr>
                                      <th>Telefones</th>
                                      <th><a href="{{route('client.add')}}" class="btn btn-primary navbar-right btn-xs"><i class="fa fa-plus"></i>  Adicionar</a></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                      <td>(85)99999-9999</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>(85)99999-9999</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>(85)99999-9999</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>(85)99999-9999</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div>
                          <div class="x_content">
                              <table class="table table-striped">
                                  <thead>
                                  <tr>
                                      <th>Emails</th>
                                      <th><a href="{{route('client.add')}}" class="btn btn-primary navbar-right btn-xs"><i class="fa fa-plus"></i>  Adicionar</a></th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  <tr>
                                      <td>dcsdarlan@email.com</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>dcsdarlan@email.com</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>dcsdarlan@email.com</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>dcsdarlan@email.com</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>dcsdarlan@email.com</td>
                                      <td>
                                          <button type="button" class="btn btn-danger navbar-right btn-xs"><i class="fa fa-trash"></i>  Apagar</button>
                                      </td>
                                  </tr>
                                  </tbody>
                              </table>
                          </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="{{route('client.list')}}" class="btn btn-primary">Cancelar</a>
                                <button type="submit" class="btn btn-success">Salvar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
